<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',  App\Http\Livewire\Attractions\Index::class)->name('list-attractions');

Route::get('/sources',  App\Http\Livewire\Sources\Index::class)->name('list-sources');

Route::get('/sources/add',  App\Http\Livewire\Sources\Add::class)->name('add-sources');

Route::get('/sources/{id}/update', App\Http\Livewire\Sources\Update::class)->name('update-sources');

Route::get('/sources/{id}/delete', App\Http\Livewire\Sources\Delete::class)->name('delete-sources');

Route::get('/attractions',  App\Http\Livewire\Attractions\Index::class)->name('list-attractions');

Route::get('/attractions/add',  App\Http\Livewire\Attractions\Add::class)->name('add-attractions');

Route::get('/attractions/{id}',  App\Http\Livewire\Attractions\View::class)->name('view-attractions');

Route::get('/attractions/{id}/update', App\Http\Livewire\Attractions\Update::class)->name('update-attractions');

Route::get('/attractions/{id}/delete', App\Http\Livewire\Attractions\Delete::class)->name('delete-attractions');

Route::get('/attraction-links/{id}',  App\Http\Livewire\AttractionLinks\Index::class)->name('list-attraction-links');

Route::get('/attraction-links/{id}/delete',  App\Http\Livewire\AttractionLinks\Delete::class)->name('delete-attraction-links');

Route::get('/areas',  App\Http\Livewire\Areas\Index::class)->name('list-areas');

Route::get('/areas/add',  App\Http\Livewire\Areas\Add::class)->name('add-areas');

Route::get('/areas/{id}',  App\Http\Livewire\Areas\View::class)->name('view-areas');

Route::get('/areas/{id}/update', App\Http\Livewire\Areas\Update::class)->name('update-areas');

Route::get('/areas/{id}/delete', App\Http\Livewire\Areas\Delete::class)->name('delete-areas');

Route::get('/lodgings',  App\Http\Livewire\Lodgings\Index::class)->name('list-lodgings');

Route::get('/lodgings/add',  App\Http\Livewire\Lodgings\Add::class)->name('add-lodgings');

Route::get('/lodgings/{id}',  App\Http\Livewire\Lodgings\View::class)->name('view-lodgings');

Route::get('/lodgings/{id}/update', App\Http\Livewire\Lodgings\Update::class)->name('update-lodgings');

Route::get('/lodgings/{id}/delete', App\Http\Livewire\Lodgings\Delete::class)->name('delete-lodgings');

Route::get('/van-life',  App\Http\Livewire\VanLife\Index::class)->name('list-van-life');

Route::get('/van-life-tags',  App\Http\Livewire\VanLife\Tags::class)->name('van-life-tags');

Route::get('/van-life-tags/{tag}',  App\Http\Livewire\VanLife\Tags::class)->name('van-life-tag');

Route::get('/van-life/add',  App\Http\Livewire\VanLife\Add::class)->name('add-van-life');

Route::get('/van-life/{id}',  App\Http\Livewire\VanLife\View::class)->name('view-van-life');

Route::get('/van-life/{id}/update', App\Http\Livewire\VanLife\Update::class)->name('update-van-life');

Route::get('/van-life/{id}/delete', App\Http\Livewire\VanLife\Delete::class)->name('delete-van-life');

Route::get('/itineraries',  App\Http\Livewire\Itineraries\Index::class)->name('list-ses');

Route::get('/itineraries-tags',  App\Http\Livewire\Itineraries\Tags::class)->name('itineraries-tags');

Route::get('/itineraries-tags/{tag}',  App\Http\Livewire\Itineraries\Tags::class)->name('itineraries-tag');

Route::get('/itineraries/add',  App\Http\Livewire\Itineraries\Add::class)->name('add-itineraries');

Route::get('/itineraries/{id}',  App\Http\Livewire\Itineraries\View::class)->name('view-itineraries');

Route::get('/itineraries/{id}/update', App\Http\Livewire\Itineraries\Update::class)->name('update-itineraries');

Route::get('/itineraries/{id}/delete', App\Http\Livewire\Itineraries\Delete::class)->name('delete-itineraries');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
  return view('dashboard');
})->name('dashboard');

Route::get('logout', function () {
  Auth::logout();
  return redirect()->route('list-attractions');
});
