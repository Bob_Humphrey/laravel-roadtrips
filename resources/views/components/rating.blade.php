<div class="">
  <div class="text-gray-700 text-sm uppercase">
    <x-label for="rating" />
  </div>
  <div class="">
    @if ($disabled)
      <x-input name="rating" wire:model="rating" class="p-2 rounded border border-gray-200 w-full appearance-none"
        disabled />
    @else
      <x-input name="rating" wire:model="rating" class="p-2 rounded border border-gray-200 w-full appearance-none" />
    @endif
  </div>
  <div class="">
    <x-error field="rating" class="text-red-500 text-sm">
      <ul>
        @foreach ($component->messages($errors) as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </x-error>
  </div>
</div>
