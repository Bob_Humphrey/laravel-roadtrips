<div class="w-4/5 justify-center pb-6 mx-auto">

  <h2 class="text-3xl text-blue-800 font-nunito_bold mb-4 text-center">
    Van Life Articles by Tag
  </h2>

  @foreach ($tags as $tag)

    <div class="my-12">
      <h3 class="text-2xl text-blue-800 font-nunito_bold mb-4 text-center border-b border-gray-300">
        {{ Str::of($tag)->title()->replace('-', ' ') }}
      </h3>

      @php
      $vanLives = $vanLivesByTag[$tag];
      @endphp

      @foreach ($vanLives as $vanLife)
        @php
        $bgColor = $loop->odd ? '' : 'bg-gray-50';
        @endphp

        @include('livewire.van-life.gridrow')

      @endforeach

    </div>

  @endforeach

</div>
