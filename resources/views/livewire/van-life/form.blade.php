    <div class="grid grid-cols-12 gap-x-8">

      {{-- FIRST COLUMN --}}

      <div class="col-span-8">

        {{-- TITLE --}}

        <div class="pb-3">
          <div class="text-gray-700 text-sm uppercase">
            <x-label for="title" />
          </div>
          <div class="">
            @if ($disabled)
              <x-input name="title" wire:model="title" class="p-2 rounded border border-gray-200 w-full appearance-none"
                disabled />
            @else
              <x-input name="title" wire:model="title"
                class="p-2 rounded border border-gray-200 w-full appearance-none" />
            @endif
          </div>
          <div class="">
            <x-error field="title" class="text-red-500 text-sm">
              <ul>
                @foreach ($component->messages($errors) as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </x-error>
          </div>
        </div>

        {{-- URL --}}

        <div class="pb-3">
          <div class="text-gray-700 text-sm uppercase">
            <x-label for="url" />
          </div>
          <div class="">
            @if ($disabled)
              <x-input name="url" wire:model="url" class="p-2 rounded border border-gray-200 w-full appearance-none"
                disabled />
            @else
              <x-input name="url" wire:model="url" class="p-2 rounded border border-gray-200 w-full appearance-none" />
            @endif
          </div>
          <div class="">
            <x-error field="url" class="text-red-500 text-sm">
              <ul>
                @foreach ($component->messages($errors) as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </x-error>
          </div>
        </div>

        {{-- NOTES --}}

        <div class="pb-3">
          <div class="text-gray-700 text-sm uppercase">
            <x-label for="notes" />
          </div>
          <div class="">
            @if ($disabled)
              <x-textarea name="content" wire:model="notes" rows="4"
                class="p-2 rounded border border-gray-200 w-full appearance-none" disabled />
            @else
              <x-textarea name="content" wire:model="notes" rows="4"
                class="p-2 rounded border border-gray-200 w-full appearance-none" />
            @endif
          </div>
          <div class="">
            <x-error field="notes" class="text-red-500 text-sm">
              <ul>
                @foreach ($component->messages($errors) as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </x-error>
          </div>
        </div>

        {{-- EXCERPT --}}

        <div class="pb-3">
          <div class="text-gray-700 text-sm uppercase">
            <x-label for="excerpt" />
          </div>
          <div class="">
            @if ($disabled)
              <x-textarea name="content" wire:model="excerpt" rows="4"
                class="p-2 rounded border border-gray-200 w-full appearance-none" disabled />
            @else
              <x-textarea name="content" wire:model="excerpt" rows="4"
                class="p-2 rounded border border-gray-200 w-full appearance-none" />
            @endif
          </div>
          <div class="">
            <x-error field="excerpt" class="text-red-500 text-sm">
              <ul>
                @foreach ($component->messages($errors) as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </x-error>
          </div>
        </div>

      </div>

      {{-- SECOND COLUMN --}}

      <div class="col-span-4">

        {{-- NEW TAG --}}

        <div class="pb-3">
          <div class="text-gray-700 text-sm uppercase">
            TAGS
          </div>
          <div class="">
            @if ($disabled)
              <x-input name="newTag" wire:model="newTag"
                class="p-2 rounded border border-gray-200 w-full appearance-none" disabled />
            @else
              <x-input name="newTag" wire:model="newTag"
                class="p-2 rounded border border-gray-200 w-full appearance-none" />
            @endif
          </div>
          <div class="">
            <x-error field="newTag" class="text-red-500 text-sm">
              <ul>
                @foreach ($component->messages($errors) as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </x-error>
          </div>
        </div>

        {{-- EXISTING TAGS --}}

        @foreach ($tags as $key => $value)
          <div class="">
            @if ($disabled)
              <x-checkbox name="tag" wire:model="tags.{{ $key }}" disabled />
            @else
              <x-checkbox name="tag" wire:model="tags.{{ $key }}" />
            @endif
            {{ $key }}
          </div>
        @endforeach

      </div>

    </div>
