      <div class="grid grid-cols-12 {{ $bgColor }}">

        <div class="col-span-2 flex justify-around">
          <div class="flex self-center h-4 w-4 hover:text-blue-500 cursor-pointer">
            <a href={{ url('/van-life/' . $vanLife->id) }}>
              <x-heroicon-o-eye />
            </a>
          </div>

          <div class="flex self-center h-4 w-4 hover:text-blue-500 cursor-pointer">
            <a href={{ url('/van-life/' . $vanLife->id . '/update') }}>
              <x-zondicon-edit-pencil />
            </a>
          </div>

          <div class="flex self-center h-4 w-4 hover:text-blue-500 cursor-pointer">
            <a href={{ url('/van-life/' . $vanLife->id . '/delete') }}>
              <x-zondicon-close />
            </a>
          </div>

          <div class="flex self-center h-4 w-4 hover:text-blue-500 cursor-pointer">
            <a href={{ $vanLife->url }} target="_blank" rel="noopener noreferrer">
              <x-heroicon-o-external-link />
            </a>
          </div>
        </div>

        <div class="col-span-7 py-1 px-4 cursor-pointer">
          <a href={{ $vanLife->url }} class="hover:text-blue-500" target="_blank" rel="noopener noreferrer">
            {{ $vanLife->title }}
          </a>
        </div>

        <div class="col-span-3 py-1 px-4 cursor-pointer">
          @foreach ($vanLife->tagged as $tag)
            <a href={{ url('/van-life-tags/' . $tag->tag_slug) }} class="hover:text-blue-500 mr-4">
              {{ $tag->tag_name }}
            </a>
          @endforeach
        </div>

      </div>
