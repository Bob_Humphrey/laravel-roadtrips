<div class="w-4/5 justify-center pb-6 mx-auto">

  <h2 class="text-3xl text-blue-800 font-nunito_bold mb-4 text-center">
    Van Life Articles
  </h2>

  <div class="">
    <a href={{ url('van-life-tags') }} class="flex justify-center w-full mb-6 text-blue-500">
      <div class="flex self-center h-5 w-5 mr-2">
        <x-heroicon-o-tag />
      </div>
      Tags
    </a>
  </div>

  @if ($vanLives)
    @foreach ($vanLives as $vanLife)
      @php
      $bgColor = $loop->odd ? '' : 'bg-gray-50';
      @endphp

      @include('livewire.van-life.gridrow')

    @endforeach
  @endif
</div>
