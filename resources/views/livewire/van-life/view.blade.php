<div class="w-2/5 justify-center pb-6 mx-auto">

  <h2 class="text-3xl text-blue-800 font-nunito_bold mb-8 text-center">
    {{ $title }}
  </h2>

  <div class="grid grid-cols-12 mb-3">
    <div class="col-span-1 flex self-center">
      <div class="flex self-center h-5 w-5">
        <x-heroicon-o-external-link />
      </div>
    </div>
    <div class="col-span-11">
      <div class="">
        <a href="{{ $url }}" class="text-blue-500" target="_blank" rel="noopener noreferrer">
          {{ $url }}
        </a>
      </div>
    </div>
  </div>

  <div class="grid grid-cols-12 mb-3">
    <div class="col-span-1 flex self-center">
      <div class="flex self-center h-5 w-5">
        <x-heroicon-o-tag />
      </div>
    </div>
    <div class="col-span-11">
      @foreach ($tags as $tag)
        <a href={{ url('/van-life-tags/' . Str::of($tag)->slug('-')) }} class="text-blue-500 mr-4">
          {{ $tag }}
        </a>
      @endforeach
    </div>
  </div>

  @if ($notes)
    <div class="grid grid-cols-12 mb-3">
      <div class="col-span-1 flex self-center">
        <div class="flex self-center h-5 w-5">
          <x-heroicon-o-document-text />
        </div>
      </div>
      <div class="col-span-11 text-justify">
        {!! nl2br($notes) !!}
      </div>
    </div>
  @endif

  <div class="grid grid-cols-12 mb-3">
    <div class="col-span-1 flex self-center">
      <div class="flex self-center h-5 w-5">
        <x-heroicon-o-annotation />
      </div>
    </div>
    <div class="col-span-11 text-justify">
      {!! nl2br($excerpt) !!}
    </div>
  </div>

  <div class="flex justify-center pt-3">
    <a href={{ url("van-life/$vanLife->id/update") }}
      class="bg-blue-500 hover:bg-blue-900 text-white text-center font-nunito_bold rounded py-2 w-1/5 cursor-pointer mx-3">
      Update
    </a>
    <a href={{ url("van-life/$vanLife->id/delete") }}
      class="bg-blue-500 hover:bg-blue-900 text-white text-center font-nunito_bold rounded py-2 w-1/5 cursor-pointer mx-3">
      Delete
    </a>
  </div>

</div>
