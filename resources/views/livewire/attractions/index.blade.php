<div class="w-4/5 justify-center pb-6 mx-auto">

  <h2 class="text-3xl text-blue-800 font-nunito_bold mb-4 text-center">
    Attractions
  </h2>

  <div class="flex justify-center w-full mb-4">
    <input type="text" wire:model="query" class="p-2 rounded border border-gray-200 w-1/4 appearance-none"
      placeholder="Search for Attractions..." />
  </div>

  @if ($attractions)
    @foreach ($attractions as $attraction)
      @php
      $bgColor = $loop->odd ? '' : 'bg-gray-50';
      @endphp

      <div class="grid grid-cols-12 {{ $bgColor }}">

        <div class="col-span-2 flex justify-around">
          <div class="flex self-center h-4 w-4 hover:text-blue-500 cursor-pointer">
            <a href={{ url('/attractions/' . $attraction->id) }}>
              <x-heroicon-o-eye />
            </a>
          </div>

          <div class="flex self-center h-4 w-4 hover:text-blue-500 cursor-pointer">
            <a href={{ url('/attractions/' . $attraction->id . '/update') }}>
              <x-zondicon-edit-pencil />
            </a>
          </div>

          <div class="flex self-center h-4 w-4 hover:text-blue-500 cursor-pointer">
            <a href={{ url('/attractions/' . $attraction->id . '/delete') }}>
              <x-zondicon-close />
            </a>
          </div>

          <div class="flex self-center h-4 w-4 hover:text-blue-500 cursor-pointer">
            <a href={{ url('/attraction-links/' . $attraction->id) }}>
              <x-heroicon-o-document-remove />
            </a>
          </div>
        </div>

        <div class="col-span-7 py-1 px-4 cursor-pointer">
          <a href={{ url('/attractions/' . $attraction->id) }} class="hover:text-blue-500">
            {{ $attraction->name }}
          </a>
        </div>

        @if ($attraction->area)
          <div class="col-span-3 py-1 px-4 cursor-pointer">
            <a href={{ url('/areas/' . $attraction->area->id) }} class="hover:text-blue-500">
              {{ $attraction->area->name }}
            </a>
          </div>
        @endif

      </div>
    @endforeach
  @endif
</div>
