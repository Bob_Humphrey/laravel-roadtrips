<div class="w-2/5 justify-center pb-6 mx-auto">

  <h2 class="text-3xl text-blue-800 font-nunito_bold mb-8 text-center">
    {{ $name }}
  </h2>

  <div class="grid grid-cols-12 mb-3">
    <div class="col-span-1 flex self-center">
      <div class="flex self-center h-5 w-5">
        <x-heroicon-o-mail />
      </div>
    </div>
    <div class="col-span-11">
      {{ $address }}
    </div>
  </div>

  <div class="grid grid-cols-12 mb-3">
    <div class="col-span-1 flex self-center">
      <div class="flex self-center h-5 w-5">
        <x-heroicon-o-at-symbol />
      </div>
    </div>
    <div class="col-span-11">
      {{ $stateName }}
    </div>
  </div>

  <div class="grid grid-cols-12 mb-3">
    <div class="col-span-1 flex self-center">
      <div class="flex self-center h-5 w-5">
        <x-heroicon-o-globe />
      </div>
    </div>
    <div class="col-span-11">
      <a href="{{ url('areas/' . $area->id) }}" class="text-blue-500">
        {{ $area->name }}
      </a>
    </div>
  </div>

  <div class="grid grid-cols-12 mb-3">
    <div class="col-span-1 flex self-center">
      <div class="flex self-center h-5 w-5">
        <x-heroicon-o-location-marker />
      </div>
    </div>
    <div class="col-span-11">
      {{ $lat }}, {{ $lng }}
    </div>
  </div>

  <div class="grid grid-cols-12 mb-3">
    <div class="col-span-1 flex self-center">
      <div class="flex self-center h-5 w-5">
        <x-heroicon-o-star />
      </div>
    </div>
    <div class="col-span-11">
      {{ $rating }}
    </div>
  </div>

  <div class="grid grid-cols-12 mb-3">
    <div class="col-span-1 flex self-center">
      <div class="flex self-center h-5 w-5">
        <x-heroicon-o-external-link />
      </div>
    </div>
    <div class="col-span-11">
      @foreach ($attractionLinks as $existingAttractionLink)
        <div class="">
          <a href="{{ $existingAttractionLink->url }}" class="text-blue-500" target="_blank" rel="noopener noreferrer">
            {{ $existingAttractionLink->title }}
          </a>
        </div>
      @endforeach
    </div>
  </div>

  <div class="grid grid-cols-12 mb-3">
    <div class="col-span-1 flex self-center">
      <div class="flex self-center h-5 w-5">
        <x-heroicon-o-annotation />
      </div>
    </div>
    <div class="col-span-11 text-justify">
      {!! nl2br($notes) !!}
    </div>
  </div>

  <div class="flex justify-center pt-3">
    <a href={{ url("attractions/$attraction_id/update") }}
      class="bg-blue-500 hover:bg-blue-900 text-white text-center font-nunito_bold rounded py-2 w-1/5 cursor-pointer mx-3">
      Update
    </a>
    <a href={{ url("attractions/$attraction_id/delete") }}
      class="bg-blue-500 hover:bg-blue-900 text-white text-center font-nunito_bold rounded py-2 w-1/5 cursor-pointer mx-3">
      Delete
    </a>
    <a href={{ url("attraction-links/$attraction_id") }}
      class="bg-blue-500 hover:bg-blue-900 text-white text-center font-nunito_bold rounded py-2 w-1/5 cursor-pointer mx-3">
      Delete Link
    </a>
  </div>

</div>
