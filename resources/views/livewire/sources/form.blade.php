    {{-- NAME --}}

    <div class="pb-3">
      <div class="text-gray-700 text-sm uppercase">
        <x-label for="name" />
      </div>
      <div class="">
        @if ($disabled)
          <x-input name="name" wire:model="name" class="p-2 rounded border border-gray-200 w-full appearance-none"
            disabled />
        @else
          <x-input name="name" wire:model="name" class="p-2 rounded border border-gray-200 w-full appearance-none" />
        @endif
      </div>
      <div class="">
        <x-error field="name" class="text-red-500 text-sm">
          <ul>
            @foreach ($component->messages($errors) as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </x-error>
      </div>
    </div>

    {{-- URL --}}

    <div class="pb-4">
      <div class="text-gray-700 text-sm uppercase">
        <x-label for="url" />
      </div>
      <div class="">
        @if ($disabled)
          <x-input name="url" wire:model="url" class="p-2 rounded border border-gray-200 w-full appearance-none"
            disabled />
        @else
          <x-input name="url" wire:model="url" class="p-2 rounded border border-gray-200 w-full appearance-none" />
        @endif
      </div>
      <div class="">
        <x-error field="url" class="text-red-500 text-sm">
          <ul>
            @foreach ($component->messages($errors) as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </x-error>
      </div>
    </div>

    {{-- DESCRIPTION --}}

    <div class="pb-4">
      <div class="text-gray-700 text-sm uppercase">
        <x-label for="description" />
      </div>
      <div class="">
        @if ($disabled)
          <x-textarea name="content" wire:model="description" rows="4"
            class="p-2 rounded border border-gray-200 w-full appearance-none" disabled />
        @else
          <x-textarea name="content" wire:model="description" rows="4"
            class="p-2 rounded border border-gray-200 w-full appearance-none" />
        @endif
      </div>
      <div class="">
        <x-error field="description" class="text-red-500 text-sm">
          <ul>
            @foreach ($component->messages($errors) as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </x-error>
      </div>
    </div>
