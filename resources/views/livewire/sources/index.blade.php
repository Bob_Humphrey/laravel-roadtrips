<div class="w-4/5 justify-center pb-6 mx-auto">

  <h2 class="text-3xl text-blue-800 font-nunito_bold mb-4 text-center">
    Sources
  </h2>

  @if ($sources)
    @foreach ($sources as $source)
      @php
      $bgColor = $loop->odd ? '' : 'bg-gray-50';
      @endphp

      <div class="grid grid-cols-12 {{ $bgColor }}">

        <div class="col-span-3 flex justify-around">
          <div class="flex self-center h-4 w-4 hover:text-blue-500 cursor-pointer">
            <a href={{ url('/sources/' . $source->id . '/update') }}>
              <x-zondicon-edit-pencil />
            </a>
          </div>

          <div class="flex self-center h-4 w-4 hover:text-blue-500 cursor-pointer">
            <a href={{ url('/sources/' . $source->id . '/delete') }}>
              <x-zondicon-close />
            </a>
          </div>
        </div>

        <div class="col-span-9 py-1 px-4 cursor-pointer">
          <a href={{ $source->url }} class="hover:text-blue-500" target="_blank" rel="noopener noreferrer">
            {{ $source->name }}
          </a>
        </div>
      </div>
    @endforeach
  @endif
</div>
