<div class="w-3/4 justify-center pb-6 mx-auto">

  <h2 class="text-3xl text-blue-800 font-nunito_bold mb-4 text-center">
    Delete Area
  </h2>

  @if (count($attractions))

    <div class="w-1/2 mb-12 mx-auto">
      <div class="font-nunito_bold mb-4">
        Cannot delete {{ $name }} because there are attractions associated with it.
      </div>

      @foreach ($attractions as $attraction)
        <div class="">
          <a href="{{ url('attractions/' . $attraction->id) }}" class="text-blue-500">
            {{ $attraction->name }}
          </a>
        </div>
      @endforeach
    </div>

  @endif

  @if (count($lodgings))

    <div class="w-1/2 mb-12 mx-auto">
      <div class="font-nunito_bold mb-4">
        Cannot delete {{ $name }} because there are lodgings associated with it.
      </div>

      @foreach ($lodgings as $lodging)
        <div class="">
          <a href="{{ url('lodgings/' . $lodging->id) }}" class="text-blue-500">
            {{ $lodging->name }}
          </a>
        </div>
      @endforeach
    </div>

  @endif

  @if ($canDelete)

    <div class="bg-gray-50 p-16 rounded border border-gray-200 mb-12">

      <div class="font-nunito_bold text-red-700 mb-4">
        Warning: This will permanently delete the area!
      </div>

      @include('livewire.areas.form', ['disabled' => 'disabled'])

      {{-- SUBMIT BUTTON --}}

      <div class="flex justify-center pt-3">
        <button type="submit" wire:click="delete"
          class="bg-blue-500 hover:bg-blue-900 text-white text-center font-nunito_bold rounded py-2 px-8 mx-auto cursor-pointer">
          Delete
        </button>
      </div>

    </div>

  @endif

</div>
