<div class="w-2/5 justify-center pb-6 mx-auto">

  <h2 class="text-3xl text-blue-800 font-nunito_bold mb-4 text-center">
    Areas
  </h2>

  @if ($areas)
    @foreach ($areas as $area)
      @php
      $bgColor = $loop->odd ? '' : 'bg-gray-50';
      @endphp

      <div class="grid grid-cols-12 {{ $bgColor }}">

        <div class="col-span-3 flex justify-around">
          <div class="flex self-center h-4 w-4 hover:text-blue-500 cursor-pointer">
            <a href={{ url('/areas/' . $area->id) }}>
              <x-heroicon-o-eye />
            </a>
          </div>

          <div class="flex self-center h-4 w-4 hover:text-blue-500 cursor-pointer">
            <a href={{ url('/areas/' . $area->id . '/update') }}>
              <x-zondicon-edit-pencil />
            </a>
          </div>

          <div class="flex self-center h-4 w-4 hover:text-blue-500 cursor-pointer">
            <a href={{ url('/areas/' . $area->id . '/delete') }}>
              <x-zondicon-close />
            </a>
          </div>
        </div>

        <div class="col-span-9 py-1 px-4 cursor-pointer">
          <a href={{ url('/areas/' . $area->id) }} class="hover:text-blue-500">
            {{ $area->name }}
          </a>
        </div>
      </div>
    @endforeach
  @endif
</div>
