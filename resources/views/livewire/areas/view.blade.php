<div class="w-2/5 justify-center pb-6 mx-auto">

  <h2 class="text-3xl text-blue-800 font-nunito_bold mb-8 text-center">
    {{ $name }}
  </h2>

  <div class="grid grid-cols-12 mb-3">
    <div class="col-span-1 flex self-center">
      <div class="flex self-center h-5 w-5">
        <x-heroicon-o-mail />
      </div>
    </div>
    <div class="col-span-11">
      {{ $address }}
    </div>
  </div>

  <div class="grid grid-cols-12 mb-3">
    <div class="col-span-1 flex self-center">
      <div class="flex self-center h-5 w-5">
        <x-heroicon-o-at-symbol />
      </div>
    </div>
    <div class="col-span-11">
      {{ $stateName }}
    </div>
  </div>

  <div class="grid grid-cols-12 mb-3">
    <div class="col-span-1 flex self-center">
      <div class="flex self-center h-5 w-5">
        <x-heroicon-o-location-marker />
      </div>
    </div>
    <div class="col-span-11">
      {{ $lat }}, {{ $lng }}
    </div>
  </div>

  <div class="grid grid-cols-12 mb-3">
    <div class="col-span-1 flex self-center">
      <div class="flex self-center h-5 w-5">
        <x-heroicon-o-exclamation-circle />
      </div>
    </div>
    <div class="col-span-11">
      @foreach ($attractions as $attraction)
        <div class="">
          <a href="{{ url('attractions/' . $attraction->id) }}" class="text-blue-500">
            {{ $attraction->name }}
          </a>
        </div>
      @endforeach
    </div>
  </div>

  <div class="grid grid-cols-12 mb-3">
    <div class="col-span-1 flex self-center">
      <div class="flex self-center h-5 w-5">
        <x-heroicon-o-office-building />
      </div>
    </div>
    <div class="col-span-11">
      @foreach ($lodgings as $lodging)
        <div class="">
          <a href="{{ url('lodgings/' . $lodging->id) }}" class="text-blue-500">
            {{ $lodging->name }}
          </a>
        </div>
      @endforeach
    </div>
  </div>

  <div class="grid grid-cols-12 mb-3">
    <div class="col-span-1 flex self-center">
      <div class="flex self-center h-5 w-5">
        <x-heroicon-o-annotation />
      </div>
    </div>
    <div class="col-span-11">
      {!! nl2br($description) !!}
    </div>
  </div>

  <div class="flex justify-center pt-3">
    <a href={{ url("areas/$area_id/update") }}
      class="bg-blue-500 hover:bg-blue-900 text-white text-center font-nunito_bold rounded py-2 w-1/5 cursor-pointer mx-3">
      Update
    </a>
    <a href={{ url("areas/$area_id/delete") }}
      class="bg-blue-500 hover:bg-blue-900 text-white text-center font-nunito_bold rounded py-2 w-1/5 cursor-pointer mx-3">
      Delete
    </a>
  </div>

</div>
