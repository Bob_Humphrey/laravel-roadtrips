    <div class="grid grid-cols-12 gap-x-8">

      {{-- FIRST COLUMN --}}

      <div class="col-span-8">

        {{-- NAME --}}

        <div class="pb-3">
          <div class="text-gray-700 text-sm uppercase">
            <x-label for="name" />
          </div>
          <div class="">
            @if ($disabled)
              <x-input name="name" wire:model="name" class="p-2 rounded border border-gray-200 w-full appearance-none"
                disabled />
            @else
              <x-input name="name" wire:model="name"
                class="p-2 rounded border border-gray-200 w-full appearance-none" />
            @endif
          </div>
          <div class="">
            <x-error field="name" class="text-red-500 text-sm">
              <ul>
                @foreach ($component->messages($errors) as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </x-error>
          </div>
        </div>

        {{-- URL --}}

        <div class="pb-3">
          <div class="text-gray-700 text-sm uppercase">
            <x-label for="url" />
          </div>
          <div class="">
            @if ($disabled)
              <x-input name="url" wire:model="url" class="p-2 rounded border border-gray-200 w-full appearance-none"
                disabled />
            @else
              <x-input name="url" wire:model="url" class="p-2 rounded border border-gray-200 w-full appearance-none" />
            @endif
          </div>
          <div class="">
            <x-error field="url" class="text-red-500 text-sm">
              <ul>
                @foreach ($component->messages($errors) as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </x-error>
          </div>
        </div>

        {{-- ADDRESS --}}

        <div class="pb-3">
          <div class="text-gray-700 text-sm uppercase">
            <x-label for="address" />
          </div>
          <div class="">
            @if ($disabled)
              <x-input name="address" wire:model="address"
                class="p-2 rounded border border-gray-200 w-full appearance-none" disabled />
            @else
              <x-input name="address" wire:model="address"
                class="p-2 rounded border border-gray-200 w-full appearance-none" />
            @endif
          </div>
          <div class="">
            <x-error field="address" class="text-red-500 text-sm">
              <ul>
                @foreach ($component->messages($errors) as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </x-error>
          </div>
        </div>

        {{-- NOTES --}}

        <div class="pb-3">
          <div class="text-gray-700 text-sm uppercase">
            <x-label for="notes" />
          </div>
          <div class="">
            @if ($disabled)
              <x-textarea name="content" wire:model="notes" rows="4"
                class="p-2 rounded border border-gray-200 w-full appearance-none" disabled />
            @else
              <x-textarea name="content" wire:model="notes" rows="4"
                class="p-2 rounded border border-gray-200 w-full appearance-none" />
            @endif
          </div>
          <div class="">
            <x-error field="notes" class="text-red-500 text-sm">
              <ul>
                @foreach ($component->messages($errors) as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </x-error>
          </div>
        </div>

      </div>

      {{-- SECOND COLUMN --}}

      <div class="col-span-4">

        {{-- TYPE --}}

        <div class="pb-3">
          <div class="text-gray-700 text-sm uppercase">
            <x-label for="type" />
          </div>

          <div class="">
            <select name="type" id="type" wire:model="type"
              class="p-2 rounded border border-gray-200 w-full appearance-none" {{ $disabled }}>
              <option value="">Pick a type...</option>
              @foreach ($lodgingTypes as $key => $value)
                <option value={{ $key }}>{{ $value }}</option>
              @endforeach
              {{-- <option value="CAM">Campground</option>
              <option value="HOT">Hotel</option>
              <option value="AIR">Airbnb</option>
              <option value="B&B">Bed and Breakfast</option>
              <option value="CAS">Casino</option> --}}
            </select>
          </div>

          <div class="">
            <x-error field="type" class="text-red-500 text-sm">
              <ul>
                @foreach ($component->messages($errors) as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </x-error>
          </div>

        </div>

        {{-- STATE --}}

        <div class="pb-3">
          <div class="text-gray-700 text-sm uppercase">
            <x-label for="state" />
          </div>

          <div class="">
            <select name="state" id="state" wire:model="state"
              class="p-2 rounded border border-gray-200 w-full appearance-none" {{ $disabled }}>
              <option value="">Pick a state...</option>
              @foreach ($states as $key => $value)
                <option value={{ $value['abbreviation'] }}>{{ $value['name'] }}</option>
              @endforeach
            </select>
          </div>

          <div class="">
            <x-error field="state" class="text-red-500 text-sm">
              <ul>
                @foreach ($component->messages($errors) as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </x-error>
          </div>

        </div>

        {{-- AREA --}}

        <div class="pb-3">
          <div class="text-gray-700 text-sm uppercase">
            <x-label for="area" />
          </div>

          <div class="">
            <select name="area" id="area" wire:model="area_id"
              class="p-2 rounded border border-gray-200 w-full appearance-none" {{ $disabled }}>
              <option value="">Pick an area...</option>
              @foreach ($areas as $key => $value)
                <option value={{ $value['id'] }}>{{ $value['name'] }}</option>
              @endforeach
            </select>
          </div>

          <div class="">
            <x-error field="area" class="text-red-500 text-sm">
              <ul>
                @foreach ($component->messages($errors) as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </x-error>
          </div>

        </div>

        {{-- COST PER NIGHT --}}

        <div class="pb-3">
          <div class="text-gray-700 text-sm uppercase">
            <x-label for="cost_per_night" />
          </div>
          <div class="">
            @if ($disabled)
              <x-input name="cost_per_night" wire:model="cost_per_night"
                class="p-2 rounded border border-gray-200 w-full appearance-none" disabled />
            @else
              <x-input name="cost_per_night" wire:model="cost_per_night"
                class="p-2 rounded border border-gray-200 w-full appearance-none" />
            @endif
          </div>
          <div class="">
            <x-error field="cost_per_night" class="text-red-500 text-sm">
              <ul>
                @foreach ($component->messages($errors) as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </x-error>
          </div>
        </div>

      </div>

    </div>
