<div class="w-4/5 justify-center pb-6 mx-auto">

  <h2 class="text-3xl text-blue-800 font-nunito_bold mb-4 text-center">
    Lodging
  </h2>

  @if ($lodgings)
    @foreach ($lodgings as $lodging)
      @php
      $bgColor = $loop->odd ? '' : 'bg-gray-50';
      @endphp

      <div class="grid grid-cols-12 {{ $bgColor }}">

        <div class="col-span-2 flex justify-around">
          <div class="flex self-center h-4 w-4 hover:text-blue-500 cursor-pointer">
            <a href={{ url('/lodgings/' . $lodging->id) }}>
              <x-heroicon-o-eye />
            </a>
          </div>

          <div class="flex self-center h-4 w-4 hover:text-blue-500 cursor-pointer">
            <a href={{ url('/lodgings/' . $lodging->id . '/update') }}>
              <x-zondicon-edit-pencil />
            </a>
          </div>

          <div class="flex self-center h-4 w-4 hover:text-blue-500 cursor-pointer">
            <a href={{ url('/lodgings/' . $lodging->id . '/delete') }}>
              <x-zondicon-close />
            </a>
          </div>

        </div>

        <div class="col-span-7 py-1 px-4 cursor-pointer">
          <a href={{ url('/lodgings/' . $lodging->id) }} class="hover:text-blue-500">
            {{ $lodging->name }}
          </a>
        </div>

        @if ($lodging->area)
          <div class="col-span-3 py-1 px-4 cursor-pointer">
            <a href={{ url('/areas/' . $lodging->area->id) }} class="hover:text-blue-500">
              {{ $lodging->area->name }}
            </a>
          </div>
        @endif

      </div>
    @endforeach
  @endif
</div>
