<div class="w-3/4 justify-center pb-6 mx-auto">

  <h2 class="text-3xl text-blue-800 font-nunito_bold mb-4 text-center">
    Add Lodging
  </h2>

  <div class="bg-gray-50 p-16 rounded border border-gray-200">

    @include('livewire.lodgings.form', ['disabled' => ''])

    {{-- SUBMIT BUTTON --}}

    <div class="flex justify-center pt-3">
      <button type="submit" wire:click="add"
        class="bg-blue-500 hover:bg-blue-900 text-white text-center font-nunito_bold rounded py-2 px-8 mx-auto cursor-pointer">
        Add
      </button>
    </div>

  </div>

</div>
