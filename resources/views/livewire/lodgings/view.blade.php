<div class="w-2/5 justify-center pb-6 mx-auto">

  <h2 class="text-3xl text-blue-800 font-nunito_bold mb-8 text-center">
    {{ $lodging->name }}
  </h2>

  <div class="grid grid-cols-12 mb-3">
    <div class="col-span-1 flex self-center">
      <div class="flex self-center h-5 w-5">
        <x-heroicon-o-office-building />
      </div>
    </div>
    <div class="col-span-11">
      {{ $typeName }}
    </div>
  </div>

  <div class="grid grid-cols-12 mb-3">
    <div class="col-span-1 flex self-center">
      <div class="flex self-center h-5 w-5">
        <x-heroicon-o-currency-dollar />
      </div>
    </div>
    <div class="col-span-11">
      ${{ $lodging->cost_per_night }}
    </div>
  </div>

  <div class="grid grid-cols-12 mb-3">
    <div class="col-span-1 flex self-center">
      <div class="flex self-center h-5 w-5">
        <x-heroicon-o-mail />
      </div>
    </div>
    <div class="col-span-11">
      {{ $lodging->address }}
    </div>
  </div>

  <div class="grid grid-cols-12 mb-3">
    <div class="col-span-1 flex self-center">
      <div class="flex self-center h-5 w-5">
        <x-heroicon-o-at-symbol />
      </div>
    </div>
    <div class="col-span-11">
      {{ $stateName }}
    </div>
  </div>

  <div class="grid grid-cols-12 mb-3">
    <div class="col-span-1 flex self-center">
      <div class="flex self-center h-5 w-5">
        <x-heroicon-o-globe />
      </div>
    </div>
    <div class="col-span-11">
      <a href="{{ url('areas/' . $lodging->area->id) }}" class="text-blue-500">
        {{ $lodging->area->name }}
      </a>
    </div>
  </div>

  <div class="grid grid-cols-12 mb-3">
    <div class="col-span-1 flex self-center">
      <div class="flex self-center h-5 w-5">
        <x-heroicon-o-location-marker />
      </div>
    </div>
    <div class="col-span-11">
      {{ $lodging->lat }}, {{ $lodging->lng }}
    </div>
  </div>

  <div class="grid grid-cols-12 mb-3">
    <div class="col-span-1 flex self-center">
      <div class="flex self-center h-5 w-5">
        <x-heroicon-o-external-link />
      </div>
    </div>
    <div class="col-span-11">
      <a href="{{ $lodging->url }}" class="text-blue-500" target="_blank" rel="noopener noreferrer">
        {{ $lodging->url }}
      </a>
    </div>
  </div>

  <div class="grid grid-cols-12 mb-3">
    <div class="col-span-1 flex self-center">
      <div class="flex self-center h-5 w-5">
        <x-heroicon-o-document-text />
      </div>
    </div>
    <div class="col-span-11 text-justify">
      {!! nl2br($lodging->notes) !!}
    </div>
  </div>

  <div class="flex justify-center pt-3">
    <a href={{ url("lodgings/$lodging->id/update") }}
      class="bg-blue-500 hover:bg-blue-900 text-white text-center font-nunito_bold rounded py-2 w-1/5 cursor-pointer mx-3">
      Update
    </a>
    <a href={{ url("lodgings/$lodging->id/delete") }}
      class="bg-blue-500 hover:bg-blue-900 text-white text-center font-nunito_bold rounded py-2 w-1/5 cursor-pointer mx-3">
      Delete
    </a>
  </div>

</div>
