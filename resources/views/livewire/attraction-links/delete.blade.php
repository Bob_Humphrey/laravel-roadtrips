<div class="w-2/5 justify-center pb-6 mx-auto">

  <h2 class="text-3xl text-blue-800 font-nunito_bold mb-4 text-center">
    Delete Attraction Link
  </h2>

  <div class="bg-gray-50 p-16 rounded border border-gray-200">

    <div class="font-nunito_bold text-red-700 mb-4">
      Warning: This will permanently delete the attraction link!
    </div>

    <h3 class="text-xl text-blue-800 font-nunito_bold mb-4">
      {{ $attraction->name }}
    </h3>

    {{ $attractionLink->title }}

    {{-- SUBMIT BUTTON --}}

    <div class="flex justify-center pt-6">
      <button type="submit" wire:click="delete"
        class="bg-blue-500 hover:bg-blue-900 text-white text-center font-nunito_bold rounded py-2 px-8 mx-auto cursor-pointer">
        Delete
      </button>
    </div>

  </div>

</div>
