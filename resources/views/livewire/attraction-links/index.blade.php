<div class="w-2/5 justify-center pb-6 mx-auto">

  <h2 class="text-3xl text-blue-800 font-nunito_bold mb-4 text-center">
    Delete Attraction Link
  </h2>

  <h3 class="text-xl text-blue-800 font-nunito_bold mb-8 text-center">
    {{ $name }}
  </h3>

  <div class="">
    @foreach ($attractionLinks as $attractionLink)
      @php
      $bgColor = $loop->odd ? '' : 'bg-gray-50';
      @endphp
      <div class="grid grid-cols-12 {{ $bgColor }} py-2">
        <div class="col-span-2 flex justify-around">
          <div class="flex self-center h-4 w-4 hover:text-blue-500 cursor-pointer">
            <a href={{ url('/attraction-links/' . $attractionLink->id . '/delete') }}>
              <x-zondicon-close />
            </a>
          </div>
        </div>
        <div class="col-span-10">
          <a href="{{ $attractionLink->url }}" class="text-blue-500" target="_blank" rel="noopener noreferrer">
            {{ $attractionLink->title }}
          </a>
        </div>
      </div>
    @endforeach
  </div>

</div>
