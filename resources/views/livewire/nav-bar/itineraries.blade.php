<div x-data="{ open: false }" class="relative">
  <div class="hover:text-blue-500 lg:px-2 py-2" @click="open = true">
    Itineraries
  </div>
  <div x-show="open" @click.away="open = false"
    class="absolute z-10 w-full bg-gray-50 text-base text-blue-800  border-t border-gray-100 rounded-t-none shadow-md">
    <div class="py-2 px-4 hover:text-blue-500">
      <a href="{{ url('/itineraries') }}">All</a>
    </div>
    @if (!empty($itineraries))
      @foreach ($itineraries as $key => $value)
        <div class="py-2 px-4 hover:text-blue-500">
          <a href="{{ url('/itineraries-tags/' . $value) }}">{{ $key }}</a>
        </div>
      @endforeach
    @endif
  </div>
</div>
