<div class="w-4/5 justify-center pb-6 mx-auto">

  <h2 class="text-3xl text-blue-800 font-nunito_bold mb-4 text-center">
    Itinerary Days by Tag
  </h2>

  @foreach ($tags as $tag)

    <div class="my-12">
      <h3 class="text-2xl text-blue-800 font-nunito_bold mb-4 text-center border-b border-gray-300">
        {{ Str::of($tag)->title()->replace('-', ' ') }}
      </h3>

      @php
      $itineraries = $itinerariesByTag[$tag];
      @endphp

      @foreach ($itineraries as $itinerary)
        @php
        $bgColor = $loop->odd ? '' : 'bg-gray-50';
        @endphp

        @include('livewire.itineraries.gridrow')

      @endforeach

    </div>

  @endforeach

  @if ($pageType === 0)

    @php
    $itineraries = $itinerariesByTag[$tag];
    @endphp

    @foreach ($itineraries as $itinerary)

      <div class="mb-16">
        <h3 class="text-2xl text-blue-800 font-nunito_bold mb-4 text-center border-b border-gray-300">
          {{ $itinerary->label . ' ' . Str::of($itinerary->area->name)->title() }}
        </h3>

        <div class="w-2/5 justify-center mx-auto">

          @if ($itinerary->lodging)
            <div class="grid grid-cols-12 pb-6">
              <div class="col-span-1 flex self-center">
                <div class="flex self-center h-5 w-5">
                  <x-heroicon-o-office-building />
                </div>
              </div>
              <div class="col-span-11">
                <div class="">
                  <a href="{{ url('lodgings/' . $itinerary->lodging->id) }}" class="text-blue-500">
                    {{ $itinerary->lodging->name }}
                  </a>
                </div>
              </div>
            </div>
          @endif

          @if ($itinerary->notes)
            <div class="grid grid-cols-12 mb-3">
              <div class="col-span-1 flex self-center">
                <div class="flex self-center h-5 w-5">
                  <x-heroicon-o-document-text />
                </div>
              </div>
              <div class="col-span-11 text-justify">
                {!! nl2br($itinerary->notes) !!}
              </div>
            </div>
          @endif

        </div>

      </div>
    @endforeach
  @endif


</div>
