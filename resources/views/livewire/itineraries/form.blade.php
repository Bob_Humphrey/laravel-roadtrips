    <div class="grid grid-cols-12 gap-x-8">

      {{-- FIRST COLUMN --}}

      <div class="col-span-6">

        {{-- LABEL --}}

        <div class="pb-3">
          <div class="text-gray-700 text-sm uppercase">
            <x-label for="label" />
          </div>
          <div class="">
            @if ($disabled)
              <x-input name="label" wire:model="label" class="p-2 rounded border border-gray-200 w-full appearance-none"
                disabled />
            @else
              <x-input name="label" wire:model="label"
                class="p-2 rounded border border-gray-200 w-full appearance-none" />
            @endif
          </div>
          <div class="">
            <x-error field="label" class="text-red-500 text-sm">
              <ul>
                @foreach ($component->messages($errors) as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </x-error>
          </div>
        </div>

        {{-- ARRIVAL DAY --}}

        <div class="pb-3">
          <div class="text-gray-700 text-sm uppercase">
            <x-label for="arrival_day" />
          </div>
          <div class="">
            @if ($disabled)
              <x-input name="arrival_day" wire:model="arrival_day"
                class="p-2 rounded border border-gray-200 w-full appearance-none" placeholder="YYYY-MM-DD" disabled />
            @else
              <x-input name="arrival_day" wire:model="arrival_day"
                class="p-2 rounded border border-gray-200 w-full appearance-none" placeholder="YYYY-MM-DD" />
            @endif
          </div>
          <div class="">
            <x-error field="arrival_day" class="text-red-500 text-sm">
              <ul>
                @foreach ($component->messages($errors) as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </x-error>
          </div>
        </div>

        {{-- DEPARTURE DAY --}}

        <div class="pb-3">
          <div class="text-gray-700 text-sm uppercase">
            <x-label for="departure_day" />
          </div>
          <div class="">
            @if ($disabled)
              <x-input name="departure_day" wire:model="departure_day"
                class="p-2 rounded border border-gray-200 w-full appearance-none" placeholder="YYYY-MM-DD" disabled />
            @else
              <x-input name="departure_day" wire:model="departure_day"
                class="p-2 rounded border border-gray-200 w-full appearance-none" placeholder="YYYY-MM-DD" />
            @endif
          </div>
          <div class="">
            <x-error field="departure_day" class="text-red-500 text-sm">
              <ul>
                @foreach ($component->messages($errors) as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </x-error>
          </div>
        </div>

        {{-- NOTES --}}

        <div class="pb-3">
          <div class="text-gray-700 text-sm uppercase">
            <x-label for="notes" />
          </div>
          <div class="">
            @if ($disabled)
              <x-textarea name="content" wire:model="notes" rows="4"
                class="p-2 rounded border border-gray-200 w-full appearance-none" disabled />
            @else
              <x-textarea name="content" wire:model="notes" rows="4"
                class="p-2 rounded border border-gray-200 w-full appearance-none" />
            @endif
          </div>
          <div class="">
            <x-error field="notes" class="text-red-500 text-sm">
              <ul>
                @foreach ($component->messages($errors) as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </x-error>
          </div>
        </div>
      </div>

      {{-- SECOND COLUMN --}}

      <div class="col-span-6">

        {{-- AREA --}}

        <div class="pb-3">
          <div class="text-gray-700 text-sm uppercase">
            <x-label for="area" />
          </div>

          <div class="">
            <select name="area" id="area" wire:model="area_id"
              class="p-2 rounded border border-gray-200 w-full appearance-none" {{ $disabled }}>
              <option value="">Pick an area...</option>
              @foreach ($areas as $key => $value)
                <option value={{ $value['id'] }}>{{ $value['name'] }}</option>
              @endforeach
            </select>
          </div>

          <div class="">
            <x-error field="area" class="text-red-500 text-sm">
              <ul>
                @foreach ($component->messages($errors) as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </x-error>
          </div>

        </div>

        {{-- LODGING --}}

        @if ($function != 'add')
          <div class="pb-3">
            <div class="text-gray-700 text-sm uppercase">
              <x-label for="lodging" />
            </div>

            <div class="">
              <select name="lodging" id="lodging" wire:model="lodging_id"
                class="p-2 rounded border border-gray-200 w-full appearance-none" {{ $disabled }}>
                <option value="">Pick a lodging...</option>
                @foreach ($lodgings as $key => $value)
                  <option value={{ $value['id'] }}>{{ $value['name'] }}</option>
                @endforeach
              </select>
            </div>

            <div class="">
              <x-error field="lodging" class="text-red-500 text-sm">
                <ul>
                  @foreach ($component->messages($errors) as $error)
                    <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </x-error>
            </div>

          </div>
        @endif

        {{-- NEW TAG --}}

        <div class="pb-3">
          <div class="text-gray-700 text-sm uppercase">
            TAGS
          </div>
          <div class="">
            @if ($disabled)
              <x-input name="newTag" wire:model="newTag"
                class="p-2 rounded border border-gray-200 w-full appearance-none" disabled />
            @else
              <x-input name="newTag" wire:model="newTag"
                class="p-2 rounded border border-gray-200 w-full appearance-none" />
            @endif
          </div>
          <div class="">
            <x-error field="newTag" class="text-red-500 text-sm">
              <ul>
                @foreach ($component->messages($errors) as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </x-error>
          </div>
        </div>

        {{-- EXISTING TAGS --}}

        @foreach ($tags as $key => $value)
          <div class="">
            @if ($disabled)
              <x-checkbox name="tag" wire:model="tags.{{ $key }}" disabled />
            @else
              <x-checkbox name="tag" wire:model="tags.{{ $key }}" />
            @endif
            {{ $key }}
          </div>
        @endforeach

      </div>

    </div>
