      <div class="grid grid-cols-12 {{ $bgColor }}">

        <div class="col-span-2 flex justify-around">
          <div class="flex self-center h-4 w-4 hover:text-blue-500 cursor-pointer">
            <a href={{ url('/itineraries/' . $itinerary->id) }}>
              <x-heroicon-o-eye />
            </a>
          </div>

          <div class="flex self-center h-4 w-4 hover:text-blue-500 cursor-pointer">
            <a href={{ url('/itineraries/' . $itinerary->id . '/update') }}>
              <x-zondicon-edit-pencil />
            </a>
          </div>

          <div class="flex self-center h-4 w-4 hover:text-blue-500 cursor-pointer">
            <a href={{ url('/itineraries/' . $itinerary->id . '/delete') }}>
              <x-zondicon-close />
            </a>
          </div>

        </div>

        <div class="col-span-2 py-1 px-4">
          {{ substr($itinerary->arrival_day, 0, 10) }}
        </div>

        <div class="col-span-2 py-1 px-4">
          {{ substr($itinerary->departure_day, 0, 10) }}
        </div>

        <div class="col-span-1 py-1 px-4 cursor-pointer">
          <a href={{ url('/itineraries/' . $itinerary->id) }} class="hover:text-blue-500" target="_blank"
            rel="noopener noreferrer">
            {{ $itinerary->label }}
          </a>
        </div>

        <div class="col-span-2 py-1 px-4">
          @if ($itinerary->area)
            <a href={{ url('/areas/' . $itinerary->area->id) }} class="hover:text-blue-500 mr-4">
              {{ $itinerary->area->name }}
            </a>
          @endif
        </div>

        <div class="col-span-3 py-1 px-4 cursor-pointer">
          @foreach ($itinerary->tagged as $tag)
            <a href={{ url('/itineraries-tags/' . $tag->tag_slug) }} class="hover:text-blue-500 mr-4">
              {{ $tag->tag_name }}
            </a>
          @endforeach
        </div>

      </div>
