<div class="w-4/5 justify-center pb-6 mx-auto">

  <h2 class="text-3xl text-blue-800 font-nunito_bold mb-4 text-center">
    Itineraries
  </h2>

  <div class="">
    <a href={{ url('itineraries-tags') }} class="flex justify-center w-full mb-6 text-blue-500">
      <div class="flex self-center h-5 w-5 mr-2">
        <x-heroicon-o-tag />
      </div>
      Tags
    </a>
  </div>

  @if ($itineraries)
    @foreach ($itineraries as $itinerary)
      @php
      $bgColor = $loop->odd ? '' : 'bg-gray-50';
      @endphp

      @include('livewire.itineraries.gridrow')

    @endforeach
  @endif
</div>
