<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description"
    content="A tool for planning road trips, built with Laravel, Tailwind CSS, Alpine JS, and Livewire.">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>{{ config('app.name', 'Laravel') }}</title>

  <!-- Fonts -->

  <!-- Styles -->
  @livewireStyles
  @bukStyles
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/pikaday/css/pikaday.css">
  <link href="{{ asset('css/all.css') }}" rel="stylesheet">
</head>

<body>

  {{ $slot }}

  <!-- Scripts -->
  @livewireScripts
  @bukScripts
  <script src="https://cdn.jsdelivr.net/npm/pikaday/pikaday.js"></script>
  <script src="{{ asset('js/all.js') }}" defer></script>
</body>

</html>
