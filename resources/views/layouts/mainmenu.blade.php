<a href="{{ url('/attractions') }}" class="hover:text-blue-500 lg:px-2 py-2">
  Attractions
</a>
<div class="flex self-center h-6 w-6 hover:text-blue-500">
  <a href={{ url('/attractions/add') }}>
    <x-heroicon-o-plus />
  </a>
</div>
<a href="{{ url('/areas') }}" class="hover:text-blue-500 lg:px-2 py-2">
  Areas
</a>
<div class="flex self-center h-6 w-6 hover:text-blue-500">
  <a href={{ url('/areas/add') }}>
    <x-heroicon-o-plus />
  </a>
</div>
<a href="{{ url('/lodgings') }}" class="hover:text-blue-500 lg:px-2 py-2">
  Lodging
</a>
<div class="flex self-center h-6 w-6 hover:text-blue-500">
  <a href={{ url('/lodgings/add') }}>
    <x-heroicon-o-plus />
  </a>
</div>
<div>
  <livewire:nav-bar.itineraries />
</div>
<div class="flex self-center h-6 w-6 hover:text-blue-500">
  <a href={{ url('/itineraries/add') }}>
    <x-heroicon-o-plus />
  </a>
</div>
<a href="{{ url('/sources') }}" class="hover:text-blue-500 lg:px-2 py-2">
  Sources
</a>
<div class="flex self-center h-6 w-6 hover:text-blue-500">
  <a href={{ url('/sources/add') }}>
    <x-heroicon-o-plus />
  </a>
</div>
<a href="{{ url('/van-life') }}" class="hover:text-blue-500 lg:px-2 py-2">
  Van Life
</a>
<div class="flex self-center h-6 w-6 hover:text-blue-500">
  <a href={{ url('/van-life/add') }}>
    <x-heroicon-o-plus />
  </a>
</div>



@guest
  <a href="{{ route('login') }}" class="hover:text-blue-500 lg:px-2 py-2">
    Login
  </a>
@endguest

@auth
  <a href="{{ url('/logout') }}" class="hover:text-blue-500 lg:px-2 py-2">
    Logout
  </a>
@endauth
