<x-alert type="success" class="bg-green-700 text-white px-20 py-4" />
<x-alert type="warning" class="bg-yellow-700 text-white px-20 py-4" />
<x-alert type="info" class="bg-blue-700 text-white px-20 py-4" />
<x-alert type="danger" class="bg-red-700 text-white px-20 py-4" />
