<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Itinerary extends Model
{
  use HasFactory;
  use \Conner\Tagging\Taggable;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'label',
    'arrival_day',
    'departure_day',
    'area_id',
    'lodging_id',
    'notes'
  ];

  /**
   * The attributes that should be cast to native types.
   *
   * @var array
   */
  protected $casts = [
    'id' => 'integer',
  ];

  /**
   * The attributes that should be mutated to dates.
   *
   * @var array
   */
  protected $dates = [
    'arrival_day',
    'departure_day',
  ];


  public function area()
  {
    return $this->belongsTo(\App\Area::class);
  }

  public function lodging()
  {
    return $this->belongsTo(\App\Lodging::class);
  }
}
