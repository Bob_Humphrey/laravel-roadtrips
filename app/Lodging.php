<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lodging extends Model
{
  use HasFactory;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'name',
    'address',
    'url',
    'type',
    'cost_per_night',
    'state',
    'lat',
    'lng',
    'area_id',
    'notes'
  ];

  /**
   * The attributes that should be cast to native types.
   *
   * @var array
   */
  protected $casts = [
    'id' => 'integer',
    'cost_per_night' => 'decimal:2',
    'lat' => 'decimal:10',
    'lng' => 'decimal:10',
    'area_id' => 'integer',
  ];


  public function area()
  {
    return $this->belongsTo(\App\Area::class);
  }

  public function itinerary()
  {
    return $this->hasMany(\App\Itinerary::class);
  }
}
