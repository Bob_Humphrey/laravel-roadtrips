<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
  use HasFactory;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'name',
    'lat',
    'lng',
    'description',
    'state',
    'address'
  ];

  /**
   * The attributes that should be cast to native types.
   *
   * @var array
   */
  protected $casts = [
    'id' => 'integer',
    'lat' => 'decimal:10',
    'lng' => 'decimal:10',
  ];


  public function attractions()
  {
    return $this->hasMany(\App\Attraction::class);
  }

  public function lodgings()
  {
    return $this->hasMany(\App\Lodging::class);
  }

  public function itinerary()
  {
    return $this->hasMany(\App\Itinerary::class);
  }
}
