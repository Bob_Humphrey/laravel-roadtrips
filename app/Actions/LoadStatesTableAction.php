<?php

namespace App\Actions;


use App\Models\State;
use Illuminate\Support\Facades\Storage;

class LoadStatesTableAction
{

  public static function execute()
  {
    $populationFile = Storage::get('population.csv');
    $records = explode("\r\n", $populationFile);
    foreach ($records as $record) {
      $fields = explode(",", $record);
      $state = new State;
      $state->abbreviation = $fields[1];
      $state->name = $fields[2];
      $state->save();
    }
  }
}
