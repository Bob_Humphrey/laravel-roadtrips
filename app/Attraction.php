<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Attraction extends Model
{
  use HasFactory;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'name',
    'address',
    'lat',
    'lng',
    'state',
    'area_id',
    'rating',
    'notes',
    'excerpt',
  ];

  /**
   * The attributes that should be cast to native types.
   *
   * @var array
   */
  protected $casts = [
    'id' => 'integer',
    'lat' => 'decimal:10',
    'lng' => 'decimal:10',
    'area_id' => 'integer',
    'rating' => 'integer',
  ];


  public function attractionLinks()
  {
    return $this->hasMany(\App\AttractionLink::class);
  }

  public function area()
  {
    return $this->belongsTo(\App\Area::class);
  }
}
