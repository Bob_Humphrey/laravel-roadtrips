<?php

function getLodgingTypes()
{
  return [
    'CAM' => 'Campground',
    'HOT' => 'Hotel',
    'AIR' => 'Airbnb',
    'B&B' => 'Bed and Breakfast',
    'CAS' => 'Casino',
  ];
}
