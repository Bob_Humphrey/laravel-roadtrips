<?php

namespace App\Http\Livewire\Itineraries;

use App\Area;
use App\Lodging;
use App\Itinerary;
use Livewire\Component;
use Illuminate\Http\Request;
use Conner\Tagging\Model\Tag;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Delete extends Component
{
  use AuthorizesRequests;

  public $itinerary;
  public $label;
  public $arrival_day;
  public $departure_day;
  public $area_id;
  public $lodging_id;
  public $notes;
  public $tags;
  public $beforeTags;
  public $newTag;
  public $areas;
  public $lodgings;

  public function delete()
  {
    $this->authorize('edit');

    $this->itinerary->delete();
    session()->flash('success', "Itinerary day has been deleted: $this->label");
    return redirect("/itineraries");
  }

  public function mount(Request $request)
  {
    $itinerary = Itinerary::find($request->id);
    $this->itinerary = $itinerary;
    $this->label = $itinerary->label;
    $this->arrival_day = substr($itinerary->arrival_day, 0, 10);
    $this->departure_day = substr($itinerary->departure_day, 0, 10);
    $this->area_id = $itinerary->area_id;
    $this->lodging_id = $itinerary->lodging_id;
    $this->notes = $itinerary->notes;

    // Areas
    $this->areas = Area::select('id', 'name')
      ->orderBy('name')
      ->get();

    // Lodgings
    if ($itinerary->area_id) {
      $this->lodgings = Lodging::select('id', 'name')
        ->where('area_id', $itinerary->area_id)
        ->orderBy('name')
        ->get();
    } else {
      $this->lodgings = Lodging::select('id', 'name')
        ->orderBy('name')
        ->get();
    }

    // Tags
    $beforeTags = [];
    foreach ($itinerary->tags as $tag) {
      $beforeTags[] = $tag->name;
    }
    $this->beforeTags = $beforeTags;

    $itineraryTags = Tag::inGroup('Itineraries')->get();
    $tags = [];
    foreach ($itineraryTags as $tag) {
      $tags[$tag->name] = in_array($tag->name, $beforeTags);
    }
    $this->tags = $tags;
  }

  public function render()
  {
    return view('livewire.itineraries.delete');
  }
}
