<?php

namespace App\Http\Livewire\Itineraries;

use App\Itinerary;
use Livewire\Component;
use Illuminate\Http\Request;

class Tags extends Component
{
  public const SINGLE_TAG = 0;
  public const ALL_TAGS = 1;
  public $pageType;
  public $itinerariesByTag;
  public $tags;

  public function mount(Request $request)
  {
    $itinerariesByTag = [];

    if ($request->tag) {
      $this->pageType = self::SINGLE_TAG;
      $itineraries = Itinerary::withAnyTag($request->tag)
        ->orderBy('label')
        ->orderBy('arrival_day')
        ->orderBy('departure_day')
        ->get();
      $itinerariesByTag[$request->tag] = $itineraries;
      $this->itinerariesByTag = $itinerariesByTag;
      $this->tags[] = $request->tag;
      return;
    }

    $this->pageType = self::ALL_TAGS;
    $tags = Itinerary::existingTags();
    foreach ($tags as $tag) {
      $itineraries = Itinerary::withAnyTag($tag->name)
        ->orderBy('label')
        ->orderBy('arrival_day')
        ->orderBy('departure_day')
        ->get();
      $itinerariesByTag[$tag->name] = $itineraries;
      $this->tags[] = $tag->name;
    }
    $this->itinerariesByTag = $itinerariesByTag;
  }

  public function render()
  {
    return view('livewire.itineraries.tags');
  }
}
