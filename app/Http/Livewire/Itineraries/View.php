<?php

namespace App\Http\Livewire\Itineraries;

use App\Area;
use App\Lodging;
use App\Itinerary;
use Livewire\Component;
use Illuminate\Http\Request;
use Conner\Tagging\Model\Tag;

class View extends Component
{
  public $itinerary;
  public $label;
  public $arrival_day;
  public $departure_day;
  public $area;
  public $area_id;
  public $lodging;
  public $lodging_id;
  public $notes;
  public $tags;
  public $areas;
  public $lodgings;

  public function mount(Request $request)
  {
    $itinerary = Itinerary::find($request->id);
    $this->itinerary = $itinerary;
    $this->label = $itinerary->label;
    $this->arrival_day = substr($itinerary->arrival_day, 0, 10);
    $this->departure_day = substr($itinerary->departure_day, 0, 10);
    $this->notes = $itinerary->notes;
    if ($itinerary->area) {
      $this->area = $itinerary->area->name;
      $this->area_id = $itinerary->area->id;
    }
    if ($itinerary->lodging) {
      $this->lodging = $itinerary->lodging->name;
      $this->lodging_id = $itinerary->lodging->id;
    }

    // Tags
    $tags = [];
    foreach ($itinerary->tags->sortBy('name') as $tag) {
      $tags[] = $tag->name;
    }
    $this->tags = $tags;
  }

  public function render()
  {
    return view('livewire.itineraries.view');
  }
}
