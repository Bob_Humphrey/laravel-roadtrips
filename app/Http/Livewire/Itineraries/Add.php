<?php

namespace App\Http\Livewire\Itineraries;

use App\Area;
use App\Itinerary;
use Livewire\Component;
use Illuminate\Support\Str;
use Conner\Tagging\Model\Tag;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Add extends Component
{
  use AuthorizesRequests;

  public $label;
  public $arrival_day;
  public $departure_day;
  public $area_id;
  public $notes;
  public $tags;
  public $newTag;
  public $areas;

  protected $rules = [
    'label' => 'required|string',
    'arrival_day' => 'nullable|date',
    'departure_day' => 'nullable|date',
    'area_id' => 'nullable|int',
    'notes' => 'nullable|string',
    'newTag' => 'nullable|string',
  ];

  public function add()
  {
    $this->authorize('edit');

    $this->validate();

    $itinerary = Itinerary::create([
      'label' => Str::title($this->label),
      'arrival_day' => $this->arrival_day,
      'departure_day' => $this->departure_day,
      'area_id' => $this->area_id,
      'notes' => $this->notes,
    ]);

    $itinerary->area = $this->area_id;

    if ($this->newTag) {
      $itinerary->tag($this->newTag);
      $tags = $itinerary->tags;
      foreach ($tags as $tag) {
        $tag->setGroup('Itineraries');
      }
    }

    foreach ($this->tags as $key => $value) {
      if ($value) {
        $itinerary->tag($key);
      }
    }

    session()->flash('success', "New itinerary day has been added: $this->label");
    return redirect("/itineraries");
  }

  public function mount()
  {
    $itineraryTags = Tag::inGroup('Itineraries')->get();
    $tags = [];
    foreach ($itineraryTags as $tag) {
      $tags[$tag->name] = false;
    }
    $this->tags = $tags;
    $this->areas = Area::select('id', 'name')->orderBy('name')->get();
  }

  public function render()
  {
    return view('livewire.itineraries.add');
  }
}
