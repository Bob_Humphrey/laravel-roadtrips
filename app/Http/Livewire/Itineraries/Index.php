<?php

namespace App\Http\Livewire\Itineraries;

use App\Itinerary;
use Livewire\Component;

class Index extends Component
{
  public $itineraries;

  public function mount()
  {
    $this->itineraries  = Itinerary::with('tagged')
      ->with('area')
      ->orderBy('arrival_day', 'desc')
      ->orderBy('departure_day', 'desc')
      ->orderBy('label')
      ->get();
  }

  public function render()
  {
    return view('livewire.itineraries.index');
  }
}
