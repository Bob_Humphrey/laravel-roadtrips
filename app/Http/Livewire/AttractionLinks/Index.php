<?php

namespace App\Http\Livewire\AttractionLinks;

use App\Attraction;
use Livewire\Component;
use Illuminate\Http\Request;

class Index extends Component
{
  public $attraction;
  public $attraction_id;
  public $name;
  // Attraction links
  public $attractionLinks;

  public function mount(Request $request)
  {
    $attraction = Attraction::find($request->id);
    $this->attraction_id = $request->id;
    $this->attraction = $attraction;
    $this->name = $attraction->name;
    $this->attractionLinks = $attraction->attractionLinks->all();
  }

  public function render()
  {
    return view('livewire.attraction-links.index');
  }
}
