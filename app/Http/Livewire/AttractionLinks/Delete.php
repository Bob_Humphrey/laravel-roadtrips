<?php

namespace App\Http\Livewire\AttractionLinks;

use App\Attraction;
use App\AttractionLink;
use Livewire\Component;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Delete extends Component
{
  use AuthorizesRequests;

  public Attraction $attraction;
  public AttractionLink $attractionLink;

  public function delete()
  {
    $this->authorize('edit');

    $id = $this->attraction->id;
    $title = $this->attractionLink->title;
    $this->attractionLink->delete();
    session()->flash('success', "Attraction Link has been updated: $title");
    return redirect(url("/attractions/$id"));
  }

  public function mount(Request $request)
  {
    $this->attractionLink = AttractionLink::find($request->id);
    $this->attraction = $this->attractionLink->attraction;
  }

  public function render()
  {
    return view('livewire.attraction-links.delete');
  }
}
