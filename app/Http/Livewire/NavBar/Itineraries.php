<?php

namespace App\Http\Livewire\NavBar;

use Livewire\Component;
use Conner\Tagging\Model\Tag;

class Itineraries extends Component
{
  public $itineraries;

  public function mount()
  {
    $this->itineraries = [];
    $tags = Tag::inGroup('Itineraries')->get();
    foreach ($tags as $tag) {
      $this->itineraries[$tag->name] = $tag->slug;
    }
  }

  public function render()
  {
    return view('livewire.nav-bar.itineraries');
  }
}
