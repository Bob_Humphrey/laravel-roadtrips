<?php

namespace App\Http\Livewire\Sources;

use App\Source;
use Livewire\Component;

class Index extends Component
{
  public $sources;

  public function mount()
  {
    $this->getSources();
  }

  public function render()
  {
    return view('livewire.sources.index');
  }

  protected function getSources()
  {
    $this->sources = Source::select('id', 'name', 'url')
      ->orderBy('name')
      ->get();
  }
}
