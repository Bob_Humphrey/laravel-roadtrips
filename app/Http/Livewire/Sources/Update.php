<?php

namespace App\Http\Livewire\Sources;

use App\Source;
use Livewire\Component;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Update extends Component
{
  use AuthorizesRequests;

  public $source;
  public $name;
  public $url;
  public $description;

  protected $rules = [
    'name' => 'required|string|min:5',
    'url' => 'required|string|url',
  ];

  public function update()
  {
    $this->authorize('edit');

    $this->validate();

    $this->source->name = Str::title($this->name);
    $this->source->url = $this->url;
    $this->source->description = $this->description;
    $this->source->save();
    session()->flash('success', "Source has been updated: $this->name");
    return redirect("/sources");
  }

  public function mount(Request $request)
  {
    $source = Source::find($request->id);
    $this->source = $source;
    $this->name = $source->name;
    $this->url = $source->url;
    $this->description = $source->description;
  }

  public function render()
  {
    return view('livewire.sources.update');
  }
}
