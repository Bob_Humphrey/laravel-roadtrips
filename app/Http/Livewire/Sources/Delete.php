<?php

namespace App\Http\Livewire\Sources;

use App\Source;
use Livewire\Component;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Delete extends Component
{
  use AuthorizesRequests;

  public $source;
  public $name;
  public $url;
  public $description;

  public function delete()
  {
    $this->authorize('edit');

    $this->source->delete();
    session()->flash('success', "Source has been deleted: $this->name");
    return redirect("/sources");
  }

  public function mount(Request $request)
  {
    $source = Source::find($request->id);
    $this->source = $source;
    $this->name = $source->name;
    $this->url = $source->url;
    $this->description = $source->description;
  }

  public function render()
  {
    return view('livewire.sources.delete');
  }
}
