<?php

namespace App\Http\Livewire\Sources;

use App\Source;
use Livewire\Component;
use Illuminate\Support\Str;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Add extends Component
{
  use AuthorizesRequests;

  public $name;
  public $url;
  public $description;

  protected $rules = [
    'name' => 'required|string|min:5',
    'url' => 'required|string|url',
  ];

  public function add()
  {
    $this->authorize('edit');

    $this->validate();

    $source = new Source;
    $source->name = Str::title($this->name);
    $source->url = $this->url;
    $source->description = $this->description;
    $source->save();
    session()->flash('success', "New source has been added: $this->name");
    return redirect("/sources");
  }

  public function render()
  {
    return view('livewire.sources.add');
  }
}
