<?php

namespace App\Http\Livewire\Attractions;

use App\Area;
use App\Attraction;
use App\Models\State;
use Livewire\Component;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Delete extends Component
{
  use AuthorizesRequests;

  public $attraction;
  public $name;
  public $address;
  public $lat;
  public $lng;
  public $state;
  public $area_id;
  public $rating;
  public $notes;
  // Attraction links
  public $attractionLinks;
  public $title;
  public $url;
  // States
  public $states;
  // Areas
  public $areas;

  public function delete()
  {
    $this->authorize('edit');

    $this->attraction->delete();
    session()->flash('success', "Attraction has been deleted: $this->name");
    return redirect("/attractions");
  }

  public function mount(Request $request)
  {
    $this->states = State::get();
    $attraction = Attraction::find($request->id);
    $this->attraction = $attraction;
    $this->name = $attraction->name;
    $this->address = $attraction->address;
    $this->lat = $attraction->lat;
    $this->lng = $attraction->lng;
    $this->state = $attraction->state;
    $this->area_id = $attraction->area_id;
    $this->rating = $attraction->rating;
    $this->notes = $attraction->notes;
    $this->areas = Area::select('id', 'name')->orderBy('name')->get()->all();
  }
  public function render()
  {
    return view('livewire.attractions.delete');
  }
}
