<?php

namespace App\Http\Livewire\Attractions;

use App\Area;
use App\Attraction;
use App\Models\State;
use App\AttractionLink;
use Livewire\Component;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Update extends Component
{
  use AuthorizesRequests;

  public $attraction;
  public $attraction_id;
  public $name;
  public $address;
  public $lat;
  public $lng;
  public $state;
  public $area_id;
  public $rating;
  public $notes;
  // Attraction links
  public $attractionLinks;
  public $title;
  public $url;
  // States
  public $states;
  // Areas
  public $areas;

  protected $rules = [
    'name' => 'required|string|min:5',
    'address' => 'nullable|string',
    'state' => 'required|string',
    'area_id' => 'nullable|integer',
    'rating' => 'required|integer|between:1,5',
    'title' => 'required_with:url',
    'url' => 'required_with:title',
  ];

  public function update()
  {
    $this->authorize('edit');

    $this->validate();

    $this->attraction->name = Str::title($this->name);
    $this->attraction->address = $this->address;
    $this->attraction->lat = $this->lat;
    $this->attraction->lng = $this->lng;
    $this->attraction->state = $this->state;
    $this->attraction->area_id = $this->area_id;
    $this->attraction->rating = $this->rating;
    $this->attraction->notes = $this->notes;
    $this->attraction->save();

    if ($this->title) {
      $attractionLink = new AttractionLink();
      $attractionLink->title = Str::title($this->title);
      $attractionLink->url = $this->url;
      $attractionLink->attraction_id = $this->attraction_id;
      $attractionLink->save();
    }

    session()->flash('success', "Attraction has been updated: $this->name");
    return redirect("/attractions/$this->attraction_id");
  }

  public function mount(Request $request)
  {
    $this->states = State::get();
    $attraction = Attraction::find($request->id);
    $this->attraction_id = $request->id;
    $this->attraction = $attraction;
    $this->name = $attraction->name;
    $this->address = $attraction->address;
    $this->lat = $attraction->lat;
    $this->lng = $attraction->lng;
    $this->state = $attraction->state;
    $this->area_id = $attraction->area_id;
    $this->rating = $attraction->rating;
    $this->notes = $attraction->notes;
    $this->attractionLinks = $attraction->attractionLinks->all();
    $this->areas = Area::select('id', 'name')->orderBy('name')->get()->all();
  }

  public function render()
  {
    return view('livewire.attractions.update');
  }
}
