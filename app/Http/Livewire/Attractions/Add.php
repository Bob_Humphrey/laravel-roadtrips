<?php

namespace App\Http\Livewire\Attractions;

use App\Area;
use App\Attraction;
use App\Models\State;
use App\AttractionLink;
use Livewire\Component;
use Illuminate\Support\Str;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Add extends Component
{
  use AuthorizesRequests;

  public $name;
  public $address;
  public $lat;
  public $lng;
  public $state;
  public $area_id;
  public $rating;
  public $notes;
  // Attraction links
  public $attractionLinks;
  public $title;
  public $url;
  // States
  public $states;
  // Areas
  public $areas;

  protected $rules = [
    'name' => 'required|string|min:5',
    'address' => 'nullable|string',
    'state' => 'required|string',
    'area_id' => 'nullable|integer',
    'rating' => 'required|integer|between:1,5',
    'title' => 'required_with:url',
    'url' => 'required_with:title',
  ];

  public function add()
  {
    $this->authorize('edit');

    $this->validate();

    $attraction = Attraction::create([
      'name' => Str::title($this->name),
      'address' => $this->address,
      'state' => $this->state,
      'rating' => $this->rating,
      'notes' => $this->notes,
      'area_id' => $this->area_id,
    ]);

    if ($this->title) {
      $attractionLink = new AttractionLink();
      $attractionLink->title = Str::title($this->title);
      $attractionLink->url = $this->url;
      $attractionLink->attraction_id = $attraction->id;
      $attractionLink->save();
    }

    session()->flash('success', "New attraction has been added: $this->name");
    return redirect("/attractions/$attraction->id");
  }

  public function mount()
  {
    $this->states = State::get();
    $this->areas = Area::select('id', 'name')->orderBy('name')->get()->all();
  }

  public function render()
  {
    return view('livewire.attractions.add');
  }
}
