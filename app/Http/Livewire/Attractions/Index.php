<?php

namespace App\Http\Livewire\Attractions;

use App\Attraction;
use Livewire\Component;

class Index extends Component
{
  public $query;
  public $attractions;

  public function updatedQuery()
  {
    $this->attractions = Attraction::with('area')
      ->where('name', 'like', '%' . $this->query . '%')
      ->orderBy('name')
      ->get();
  }

  public function mount()
  {
    $this->getAttractions();
  }

  public function render()
  {
    return view('livewire.attractions.index');
  }

  protected function getAttractions()
  {
    $this->attractions = Attraction::with('area')
      ->orderBy('name')
      ->get();
  }
}
