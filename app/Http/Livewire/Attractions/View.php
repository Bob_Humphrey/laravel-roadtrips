<?php

namespace App\Http\Livewire\Attractions;

use App\Area;
use App\Attraction;
use App\Models\State;
use Livewire\Component;
use Illuminate\Http\Request;

class View extends Component
{
  public $attraction;
  public $attraction_id;
  public $name;
  public $address;
  public $lat;
  public $lng;
  public $state;
  public $area_id;
  public $rating;
  public $notes;
  // Attraction links
  public $attractionLinks;
  public $title;
  public $url;
  // States
  public $states;
  public $stateName;
  // Areas
  public $area;
  public $areaName;

  public function mount(Request $request)
  {
    $this->states = State::get();
    $attraction = Attraction::find($request->id);
    $this->attraction_id = $request->id;
    $this->attraction = $attraction;
    $this->name = $attraction->name;
    $this->address = $attraction->address;
    $this->lat = $attraction->lat;
    $this->lng = $attraction->lng;
    $this->state = $attraction->state;
    $this->area_id = $attraction->area_id;
    $this->rating = $attraction->rating;
    $this->notes = $attraction->notes;
    $this->attractionLinks = $attraction->attractionLinks->all();
    $state = State::where('abbreviation', $attraction->state)->first();
    $this->stateName = $state->name;
    $this->area = Area::find($attraction->area_id);
    $this->areaName = $this->area->name;
  }
  public function render()
  {
    return view('livewire.attractions.view');
  }
}
