<?php

namespace App\Http\Livewire\VanLife;

use App\VanLife;
use Livewire\Component;
use Illuminate\Http\Request;
use Conner\Tagging\Model\Tag;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Delete extends Component
{
  use AuthorizesRequests;

  public $vanLife;
  public $title;
  public $url;
  public $notes;
  public $excerpt;
  public $tags;
  public $beforeTags;
  public $newTag;

  public function delete()
  {
    $this->authorize('edit');

    $this->vanLife->delete();
    session()->flash('success', "Van life article has been deleted: $this->title");
    return redirect("/van-life");
  }

  public function mount(Request $request)
  {
    $vanLife = VanLife::find($request->id);
    $this->vanLife = $vanLife;
    $this->title = $vanLife->title;
    $this->url = $vanLife->url;
    $this->notes = $vanLife->notes;
    $this->excerpt = $vanLife->excerpt;

    $beforeTags = [];
    foreach ($vanLife->tags as $tag) {
      $beforeTags[] = $tag->name;
    }
    $this->beforeTags = $beforeTags;

    $vanTags = Tag::inGroup('VanLife')->get();
    $tags = [];
    foreach ($vanTags as $tag) {
      $tags[$tag->name] = in_array($tag->name, $beforeTags);
    }
    $this->tags = $tags;
  }

  public function render()
  {
    return view('livewire.van-life.delete');
  }
}
