<?php

namespace App\Http\Livewire\VanLife;

use App\VanLife;
use Livewire\Component;

class Index extends Component
{
  public $vanLives;

  public function mount()
  {
    $this->getVanLives();
  }

  public function render()
  {
    return view('livewire.van-life.index');
  }

  protected function getVanLives()
  {
    $this->vanLives = VanLife::with('tagged')
      ->orderBy('title')
      ->get();
  }
}
