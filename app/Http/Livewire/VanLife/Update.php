<?php

namespace App\Http\Livewire\VanLife;

use App\VanLife;
use Livewire\Component;
use Illuminate\Http\Request;
use Conner\Tagging\Model\Tag;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Update extends Component
{
  use AuthorizesRequests;

  public $vanLife;
  public $title;
  public $url;
  public $notes;
  public $excerpt;
  public $tags;
  public $beforeTags;
  public $newTag;

  protected $rules = [
    'title' => 'required|string|min:5',
    'url' => 'required|url',
    'notes' => 'nullable|string',
    'excerpt' => 'nullable|string',
    'newTag' => 'nullable|string',
  ];

  public function update()
  {
    $this->authorize('edit');

    $this->validate();

    $this->vanLife->title = $this->title;
    $this->vanLife->url = $this->url;
    $this->vanLife->notes = $this->notes;
    $this->vanLife->excerpt = $this->excerpt;
    $this->vanLife->save();

    $afterTags = [];
    foreach ($this->tags as $key => $value) {
      if ($value) {
        $afterTags[] = $key;
      }
    }

    if ($this->beforeTags == $afterTags) {
      // No change in tags. No need to update tags.
    } else {
      $this->vanLife->untag();
      foreach ($afterTags as $tag) {
        $this->vanLife->tag($tag);
      }
    }

    if ($this->newTag) {
      $this->vanLife->tag($this->newTag);
      $tags = $this->vanLife->tags;
      foreach ($tags as $tag) {
        $tag->setGroup('VanLife');
      }
    }

    session()->flash('success', "Van life article has been updated: $this->title");
    return redirect("/van-life");
  }

  public function mount(Request $request)
  {
    $vanLife = VanLife::find($request->id);
    $this->vanLife = $vanLife;
    $this->title = $vanLife->title;
    $this->url = $vanLife->url;
    $this->notes = $vanLife->notes;
    $this->excerpt = $vanLife->excerpt;

    $beforeTags = [];
    foreach ($vanLife->tags as $tag) {
      $beforeTags[] = $tag->name;
    }
    $this->beforeTags = $beforeTags;

    $vanTags = Tag::inGroup('VanLife')->get();
    $tags = [];
    foreach ($vanTags as $tag) {
      $tags[$tag->name] = in_array($tag->name, $beforeTags);
    }
    $this->tags = $tags;
  }

  public function render()
  {
    return view('livewire.van-life.update');
  }
}
