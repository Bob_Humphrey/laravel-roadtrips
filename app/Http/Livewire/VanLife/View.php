<?php

namespace App\Http\Livewire\VanLife;

use App\VanLife;
use Livewire\Component;
use Illuminate\Http\Request;
use Conner\Tagging\Model\Tag;

class View extends Component
{
  public $vanLife;
  public $title;
  public $url;
  public $notes;
  public $excerpt;
  public $tags;

  public function mount(Request $request)
  {
    $vanLife = VanLife::find($request->id);
    $this->vanLife = $vanLife;
    $this->title = $vanLife->title;
    $this->url = $vanLife->url;
    $this->notes = $vanLife->notes;
    $this->excerpt = $vanLife->excerpt;

    $tags = [];
    foreach ($vanLife->tags->sortBy('name') as $tag) {
      $tags[] = $tag->name;
    }
    $this->tags = $tags;
  }

  public function render()
  {
    return view('livewire.van-life.view');
  }
}
