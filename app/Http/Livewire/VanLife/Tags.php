<?php

namespace App\Http\Livewire\VanLife;

use App\VanLife;
use Livewire\Component;
use Illuminate\Http\Request;

class Tags extends Component
{
  public $vanLivesByTag;
  public $tags;

  public function mount(Request $request)
  {
    $vanLivesByTag = [];

    if ($request->tag) {
      $vanLives = VanLife::withAnyTag($request->tag)
        ->orderBy('title')
        ->get();
      $vanLivesByTag[$request->tag] = $vanLives;
      $this->vanLivesByTag = $vanLivesByTag;
      $this->tags[] = $request->tag;
      return;
    }

    $tags = VanLife::existingTags();
    foreach ($tags as $tag) {
      $vanLives = VanLife::withAnyTag($tag->name)
        ->orderBy('title')
        ->get();
      $vanLivesByTag[$tag->name] = $vanLives;
      $this->tags[] = $tag->name;
    }
    $this->vanLivesByTag = $vanLivesByTag;
  }

  public function render()
  {
    return view('livewire.van-life.tags');
  }
}
