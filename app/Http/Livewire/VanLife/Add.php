<?php

namespace App\Http\Livewire\VanLife;

use App\VanLife;
use Livewire\Component;
use Illuminate\Support\Str;
use Conner\Tagging\Model\Tag;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Add extends Component
{
  use AuthorizesRequests;

  public $title;
  public $url;
  public $notes;
  public $excerpt;
  public $tags;
  public $newTag;

  protected $rules = [
    'title' => 'required|string|min:5',
    'url' => 'required|url',
    'notes' => 'nullable|string',
    'excerpt' => 'nullable|string',
    'newTag' => 'nullable|string',
  ];

  public function add()
  {
    $this->authorize('edit');

    $this->validate();

    $vanLife = VanLife::create([
      'title' => Str::title($this->title),
      'url' => $this->url,
      'notes' => $this->notes,
      'excerpt' => $this->excerpt,
    ]);

    if ($this->newTag) {
      $vanLife->tag($this->newTag);
      $tags = $vanLife->tags;
      foreach ($tags as $tag) {
        $tag->setGroup('VanLife');
      }
    }

    foreach ($this->tags as $key => $value) {
      if ($value) {
        $vanLife->tag($key);
      }
    }

    session()->flash('success', "New van life article has been added: $this->title");
    return redirect("/van-life");
  }

  public function mount()
  {
    $vanTags = Tag::inGroup('VanLife')->get();
    $tags = [];
    foreach ($vanTags as $tag) {
      $tags[$tag->name] = false;
    }
    $this->tags = $tags;
  }

  public function render()
  {
    return view('livewire.van-life.add');
  }
}
