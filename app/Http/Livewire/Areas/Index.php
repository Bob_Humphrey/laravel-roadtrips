<?php

namespace App\Http\Livewire\Areas;

use App\Area;
use Livewire\Component;

class Index extends Component
{
  public $areas;

  public function mount()
  {
    $this->getAreas();
  }

  public function render()
  {
    return view('livewire.areas.index');
  }

  protected function getAreas()
  {
    $this->areas = Area::select('id', 'name')
      ->orderBy('name')
      ->get();
  }
}
