<?php

namespace App\Http\Livewire\Areas;

use App\Area;
use App\Models\State;
use Livewire\Component;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Add extends Component
{
  use AuthorizesRequests;

  public $name;
  public $address;
  public $lat;
  public $lng;
  public $state;
  public $description;
  // States
  public $states;

  protected $rules = [
    'name' => 'required|string|min:3',
    'address' => 'nullable|string',
    'state' => 'required|string',
    'description' => 'nullable|string',
  ];

  public function add()
  {
    $this->authorize('edit');

    $this->validate();

    $area = Area::create([
      'name' => Str::title($this->name),
      'address' => $this->address,
      'state' => $this->state,
      'description' => $this->description,
    ]);

    session()->flash('success', "New areas has been added: $this->name");
    // return redirect("/areas/$area->id");
    return redirect("/areas");
  }

  public function mount()
  {
    $this->states = State::get();
  }

  public function render()
  {
    return view('livewire.areas.add');
  }
}
