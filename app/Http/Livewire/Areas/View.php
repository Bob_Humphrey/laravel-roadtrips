<?php

namespace App\Http\Livewire\Areas;

use App\Area;
use App\Models\State;
use Livewire\Component;
use Illuminate\Http\Request;

class View extends Component
{
  public $area;
  public $area_id;
  public $name;
  public $address;
  public $lat;
  public $lng;
  public $state;
  public $description;
  // States
  public $states;
  public $stateName;
  // Attractions
  public $attractions;
  // Lodging
  public $lodgings;

  public function mount(Request $request)
  {
    $this->states = State::get();
    $area = Area::find($request->id);
    $this->area_id = $request->id;
    $this->area = $area;
    $this->name = $area->name;
    $this->address = $area->address;
    $this->lat = $area->lat;
    $this->lng = $area->lng;
    $this->state = $area->state;
    $this->description = $area->description;
    $state = State::where('abbreviation', $area->state)->first();
    $this->stateName = $state->name;
    $this->attractions = $area->attractions->sortBy('name');
    $this->lodgings = $area->lodgings;
  }

  public function render()
  {
    return view('livewire.areas.view');
  }
}
