<?php

namespace App\Http\Livewire\Areas;

use App\Area;
use App\Models\State;
use Livewire\Component;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Update extends Component
{
  use AuthorizesRequests;

  public $area;
  public $area_id;
  public $name;
  public $address;
  public $lat;
  public $lng;
  public $state;
  public $description;
  // States
  public $states;

  protected $rules = [
    'name' => 'required|string|min:3',
    'address' => 'nullable|string',
    'state' => 'required|string',
    'description' => 'nullable|string',
  ];

  public function update()
  {
    $this->authorize('edit');

    $this->validate();

    $this->area->name = Str::title($this->name);
    $this->area->address = $this->address;
    $this->area->state = $this->state;
    $this->area->description = $this->description;
    $this->area->save();

    session()->flash('success', "Area has been updated: $this->name");
    // return redirect("/areas/$area->id");
    return redirect("/areas");
  }

  public function mount(Request $request)
  {
    $this->states = State::get();
    $area = Area::find($request->id);
    $this->area_id = $request->id;
    $this->area = $area;
    $this->name = $area->name;
    $this->address = $area->address;
    $this->lat = $area->lat;
    $this->lng = $area->lng;
    $this->state = $area->state;
    $this->description = $area->description;
  }

  public function render()
  {
    return view('livewire.areas.update');
  }
}
