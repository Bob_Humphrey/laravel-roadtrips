<?php

namespace App\Http\Livewire\Lodgings;

use App\Area;
use App\Lodging;
use App\Models\State;
use Livewire\Component;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Update extends Component
{
  use AuthorizesRequests;

  public $lodging;
  public $lodging_id;
  public $name;
  public $address;
  public $url;
  public $type;
  public $cost_per_night;
  public $lat;
  public $lng;
  public $state;
  public $area_id;
  public $notes;
  // States
  public $states;
  public $stateName;
  // Areas
  public $areas;
  public $areaName;
  // Lodging Types
  public $lodgingTypes;

  protected $rules = [
    'name' => 'required|string|min:5',
    'address' => 'nullable|string',
    'url' => 'nullable|string',
    'type' => 'required|string|size:3',
    'state' => 'required|string|size:2',
    'cost_per_night' => 'nullable|numeric',
    'area_id' => 'nullable|integer',
    'notes' => 'nullable|string',
  ];

  public function update()
  {
    $this->authorize('edit');

    $this->validate();

    $this->lodging->name = $this->name;
    $this->lodging->address = $this->address;
    $this->lodging->url = $this->url;
    $this->lodging->type = $this->type;
    $this->lodging->cost_per_night = $this->cost_per_night;
    $this->lodging->state = $this->state;
    $this->lodging->notes = $this->notes;
    $this->lodging->area_id = $this->area_id;
    $this->lodging->save();

    session()->flash('success', "Lodging has been updated: $this->name");
    return redirect('/lodgings/' . $this->lodging_id);
  }

  public function mount(Request $request)
  {
    $lodging = Lodging::find($request->id);
    $this->lodging = $lodging;
    $this->lodging_id = $lodging->id;
    $this->name = $lodging->name;
    $this->address = $lodging->address;
    $this->url = $lodging->url;
    $this->type = $lodging->type;
    $this->cost_per_night = $lodging->cost_per_night;
    $this->lat = $lodging->lat;
    $this->lng = $lodging->lng;
    $this->state = $lodging->state;
    $this->area_id = $lodging->area_id;
    $this->notes = $lodging->notes;
    $this->states = State::get();
    $this->areas = Area::select('id', 'name')->orderBy('name')->get()->all();
    $this->lodgingTypes = getLodgingTypes();
  }

  public function render()
  {
    return view('livewire.lodgings.update');
  }
}
