<?php

namespace App\Http\Livewire\Lodgings;

use App\Area;
use App\Lodging;
use App\Models\State;
use Livewire\Component;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Delete extends Component
{
  use AuthorizesRequests;

  public $lodging;
  public $name;
  public $address;
  public $url;
  public $type;
  public $cost_per_night;
  public $lat;
  public $lng;
  public $state;
  public $area_id;
  public $notes;
  // States
  public $states;
  // Areas
  public $areas;
  // Lodging Types
  public $lodgingTypes;

  public function mount(Request $request)
  {
    $lodging = Lodging::find($request->id);
    $this->lodging = $lodging;
    $this->name = $lodging->name;
    $this->address = $lodging->address;
    $this->url = $lodging->url;
    $this->type = $lodging->type;
    $this->cost_per_night = $lodging->cost_per_night;
    $this->lat = $lodging->lat;
    $this->lng = $lodging->lng;
    $this->state = $lodging->state;
    $this->area_id = $lodging->area_id;
    $this->notes = $lodging->notes;
    $this->states = State::get();
    $this->areas = Area::select('id', 'name')->orderBy('name')->get()->all();
    $this->lodgingTypes = getLodgingTypes();
  }

  public function delete()
  {
    $this->authorize('edit');

    $this->lodging->delete();
    session()->flash('success', "Lodging has been deleted: $this->name");
    return redirect("/lodgings");
  }

  public function render()
  {
    return view('livewire.lodgings.delete');
  }
}
