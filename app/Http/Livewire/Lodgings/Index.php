<?php

namespace App\Http\Livewire\Lodgings;

use App\Lodging;
use Livewire\Component;

class Index extends Component
{
  public $lodgings;

  public function mount()
  {
    $this->getLodgings();
  }

  public function render()
  {
    return view('livewire.lodgings.index');
  }

  protected function getLodgings()
  {
    $this->lodgings = Lodging::with('area')
      ->orderBy('name')
      ->get();
  }
}
