<?php

namespace App\Http\Livewire\Lodgings;

use App\Area;
use App\Lodging;
use App\Models\State;
use Livewire\Component;
use Illuminate\Http\Request;

class View extends Component
{
  public $lodging;
  public $areaName;
  public $stateName;
  public $typeName;
  public $lodgingTypes;

  public function mount(Request $request)
  {
    $this->lodging = Lodging::find($request->id);
    $state = State::where('abbreviation', $this->lodging->state)->first();
    $this->stateName = $state->name;
    $area = Area::find($this->lodging->area_id);
    $this->areaName = $area->name;
    $this->lodgingTypes = getLodgingTypes();
    $this->typeName = $this->lodgingTypes[$this->lodging->type];
  }

  public function render()
  {
    return view('livewire.lodgings.view');
  }
}
