<?php

namespace App\Http\Livewire\Lodgings;

use App\Area;
use App\Lodging;
use App\Models\State;
use App\AttractionLink;
use Livewire\Component;
use Illuminate\Support\Str;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Add extends Component
{
  use AuthorizesRequests;

  public $name;
  public $address;
  public $url;
  public $type;
  public $cost_per_night;
  public $lat;
  public $lng;
  public $state;
  public $area_id;
  public $notes;
  // States
  public $states;
  // Areas
  public $areas;
  // Lodging Types
  public $lodgingTypes;

  protected $rules = [
    'name' => 'required|string|min:5',
    'address' => 'nullable|string',
    'url' => 'nullable|string',
    'type' => 'required|string|size:3',
    'state' => 'required|string|size:2',
    'cost_per_night' => 'nullable|numeric',
    'area_id' => 'nullable|integer',
    'notes' => 'nullable|string',
  ];

  public function add()
  {
    $this->authorize('edit');

    $this->validate();

    $lodging = Lodging::create([
      'name' => Str::title($this->name),
      'address' => $this->address,
      'url' => $this->url,
      'type' => $this->type,
      'cost_per_night' => $this->cost_per_night,
      'state' => $this->state,
      'notes' => $this->notes,
      'area_id' => $this->area_id,
    ]);

    session()->flash('success', "New lodging has been added: $this->name");
    return redirect("/lodgings/$lodging->id");
  }

  public function mount()
  {
    $this->states = State::get();
    $this->areas = Area::select('id', 'name')->orderBy('name')->get()->all();
    $this->lodgingTypes = getLodgingTypes();
  }
  public function render()
  {
    return view('livewire.lodgings.add');
  }
}
