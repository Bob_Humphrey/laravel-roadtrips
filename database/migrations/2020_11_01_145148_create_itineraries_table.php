<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItinerariesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('itineraries', function (Blueprint $table) {
      $table->id();
      $table->string('label', 100)->nullable()->index();
      $table->date('arrival_day')->nullable()->index();
      $table->date('departure_day')->nullable()->index();
      $table->foreignId('area_id')->nullable()->index();
      $table->foreignId('lodging_id')->nullable()->index();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('itineraries');
  }
}
