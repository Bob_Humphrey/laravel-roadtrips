<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLodgingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lodgings', function (Blueprint $table) {
            $table->id();
            $table->string('name', 100);
            $table->string('url', 200)->nullable();
            $table->char('type', 3);
            $table->decimal('cost_per_night', 5, 2)->default(0);
            $table->string('address', 200);
            $table->decimal('lat', 13, 10)->default(0);
            $table->decimal('lng', 13, 10)->default(0);
            $table->unsignedBigInteger('area_id')->index()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lodgings');
    }
}
