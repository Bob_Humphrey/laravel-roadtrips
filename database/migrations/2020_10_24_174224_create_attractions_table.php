<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttractionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attractions', function (Blueprint $table) {
            $table->id();
            $table->string('name', 100);
            $table->string('address', 200)->nullable();
            $table->decimal('lat', 13, 10)->default(0);
            $table->decimal('lng', 13, 10)->default(0);
            $table->unsignedBigInteger('area_id')->index()->nullable();
            $table->tinyInteger('rating')->index()->default(0);
            $table->text('notes')->nullable();
            $table->text('excerpt')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attractions');
    }
}
